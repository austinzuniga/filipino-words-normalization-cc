from nltk.translate.bleu_score import sentence_bleu

reference = open("normalized.txt", 'r').readlines()
candidate = open("output.txt", 'r').readlines()

if len(reference) != len(candidate):
    raise ValueError('The number of sentences in both files do not match.')

score = 0.
score_2gram = 0.
score_3gram = 0.
score_4gram = 0.
for i in range(len(reference)):
    score += sentence_bleu([reference[i].strip().split()], candidate[i].strip().split(), weights=(1, 0, 0, 0))
    #score_2gram += sentence_bleu([reference[i].strip().split()], candidate[i].strip().split(), weights=(0, 1, 0, 0))
    #score_3gram += sentence_bleu([reference[i].strip().split()], candidate[i].strip().split(), weights=(0, 0, 1, 0))
    #score_4gram += sentence_bleu([reference[i].strip().split()], candidate[i].strip().split(), weights=(0, 0, 0, 1))

score /= len(reference)
#score_2gram /= len(reference)
#score_3gram /= len(reference)
#score_4gram /= len(reference)

print("\n\n\nThe bleu score 1-gram is: "+str(score))
#print("The bleu score 2-gram is: "+str(score_2gram))
#print("The bleu score 3-gram is: "+str(score_3gram))
#print("The bleu score 4-gram is: "+str(score_4gram))