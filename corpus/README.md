# Filipino Language Corpus
This folder contains Filipino Language Corpus collected from different sources like Wikipedia, Palito and seasite. 

### Language model Corpus:
A collection of Filipino sentences.

#### Wordlist:
Contains list of words in filipino language.

#### Parallel Corpus
Consists of two monolingual corpora. Filipino and Jejemon Words

#### Uncleaned Corpus
A collection of Filipino sentences and wordlist that has not been cleaned.
