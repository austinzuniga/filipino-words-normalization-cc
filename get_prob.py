import os
from six.moves import cPickle

def select_best_word(get_probability):
    with open('lstm-word-level-language-model/save/config.pkl', 'rb') as f:
        saved_args = cPickle.load(f)
    with open('lstm-word-level-language-model/save/words_vocab.pkl', 'rb') as f:
        words, vocab = cPickle.load(f)
    result = list()
    for word in get_probability.split():
       result.append(word+"-"+str(vocab.get(word, 0)))
       
    max_element = [vocab.get(word, 0) for word in get_probability.split()]
    for word in result:
        word = word.split("-")
        if int(word[1]) == max(max_element):
            return word[0] 