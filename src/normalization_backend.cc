#include "normalization_backend.h"
#include "double_metaphone.h"
#include <iostream>

// getting all words in file and storing in trie wordlist
normalize :: normalize(){
	total_words=0;
	wordlist=new trie();
	std::ifstream fin;
	fin.open("corpus/Wordlist/Filipino_wordlist.txt"); // Filipino-wordlist.txt Filipino_words.txt
	std::string word_in_file;
	while(fin.eof()==0){
		fin>>word_in_file;
		word_in_file=preprocess(word_in_file);
		if(word_in_file!=""){
			add_word(wordlist,word_in_file); // adding words to trie
			total_words++; // total words added to trie
		}
	}
	fin.close();
	load_rule_base();
	load_oov_wordlist();
}
void normalize :: load_oov_wordlist(){
	oov_wordlist=new trie();
	std::ifstream fin;
	fin.open("corpus/Wordlist/Filipino_wordlist_OOV.txt"); // Filipino-wordlist.txt Filipino_words.txt
	std::string word_in_file;
	while(fin.eof()==0){
		fin>>word_in_file;
		word_in_file=preprocess(word_in_file);
		if(word_in_file!=""){
			add_word(oov_wordlist,word_in_file); // adding words to trie
		}
	}
	fin.close();
}
// loading rule based data
void normalize :: load_rule_base(){
	std::ifstream infile("rule_based.cfg");
	std::string line;
	while( std::getline(infile, line) ){
		std::stringstream is_line(line);
		std::string key;
		if( std::getline(is_line, key, '=') ){
	  		std::string value;
	    	if( std::getline(is_line, value) ){
	    		rule_based_data.push_back(std::make_pair(key,value)); // storing rules to var
	  		}
	 	}
	}
	infile.close();
}
// returns no. of unable to normalize word
std::string normalize :: error_model(){
	std::string error = std::to_string(unable_to_normalize_words); // convert int to string
	unable_to_normalize_words = 0; // reset error count
	return error;
}

// returns full report of normalization
std::string normalize :: report_model(){
	std::string error = std::to_string(unable_to_normalize_words); // convert int to string
	std::string no_success_lstm = std::to_string(no_normalized_words_lstm);
	std::string no_success_numbers = std::to_string(no_normalized_words_numbers);
	std::string no_success_indictionary = std::to_string(in_dictionary);
	std::string no_words_data = std::to_string(no_words);

	return "\ntotal processed words: "+no_words_data+"\nNormalized by LSTM: "
	+no_success_lstm+"\n# of normalized numbers: "+no_success_numbers+"\nIn Dictionary/substitution: "+no_success_indictionary+"\nError Count: "+error;
}

// return average time for normalization process per sentence
std::string normalize :: average_time(){
	float time = average_time_taken / no_words;
	std::string duration_con = std::to_string(time); // convert int to string
	average_time_taken = 0;
	no_words = 0;
	return duration_con;
}
// function to remove all characters except alphabet and numbers and transform to lowercase
std::string normalize :: preprocess(std::string word){
	std::string clean;
	int word_size = word.size();
    for(int i = 0; i < word_size ; ++i){ //remove non alphabet and number
        if (((word[i] >= 'a' && word[i]<='z') || (word[i] >= 'A' && word[i]<='Z') || isdigit(word[i]) 
        	|| word[i] == '@' || 
        	
        	((word[i] == '?' || word[i] == '!' || word[i] == '.' || word[i] == ','|| word[i] == '-' ||
        	word[i] == '%' || word[i] == '"' || word[i] == ']'|| word[i] == '}'
        	|| word[i] == ')')
        	and i == word_size-1
        		)
        	||
        	((word[i] == '"' || word[i] == '['|| word[i] == '{'
        	|| word[i] == '(')
        	and i == 0
        		)

        	)) {
            clean+= tolower(word[i]);
        }
    }
    return clean;
} 

// adding word to trie
void normalize :: add_word(struct trie *wordlist,std::string word){
	struct trie *vertex = wordlist; 
	int word_lenght = word.length();
	for (int i = 0; i < word_lenght; i++) { 
		int index = word[i] - 'a'; 
        if (!vertex->edges[index]) 
            vertex->edges[index] = new trie(); 
  
        vertex = vertex->edges[index]; 
    }
        vertex->words = true;  
}
// search word if in trie. returns 0 or 1
long long normalize :: search(struct trie *wordlist,std::string word){
	struct trie *vertex = wordlist; 
	int word_lenght = word.length();
	for (int i = 0; i < word_lenght; i++) { 
		int index = word[i] - 'a';
		if(index > 26)
			break; 
        if (!vertex->edges[index]) 
            return false; 
        vertex = vertex->edges[index];
    } 
    return (vertex != NULL && vertex->words); 
}
// check if string is all number
bool is_number(const std::string& s){
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

// for repeating character or string
std::string normalize :: repeat_words(std::string str) { 
    std::vector<std::string> possible;
	std::vector<std::string> :: iterator it;
    int i = 0;
	std::string rep;
	int word_size = str.size();
	std::string after="";
	int indicator=0;
	while( i < word_size){
		if(isdigit(str[i])){
			std::string com;
			for(int i = rep.size(); i != -1; i--){
				com=rep[i] + com;
				std::string s1,s2 = com;
				indicator = 1;
				int n = str[i] - '0' - 1; 
				for (int j=1; j<n;j++){ 
	    			s2 += s1; // Concatinating strings
	    		} 
	    		if(i!=0)
	    			possible.push_back(rep+s2);
			}
		}
		else{
			if(indicator)
				after+=str[i];
			else
				rep+=str[i];
		}
		i++;
	}
	std::string to_return;
	int indi = 0;
	for(it=possible.begin();it!=possible.end();it++){
		std::string test = (*it).c_str(); 
		test = test + after;
		if(search(wordlist,test)){
			to_return = test;
			indi = 1;
			//break;
		}
	}
	if(!indi)
		to_return = "";
    return to_return; 
}

// for replacing character or string  example: ReplaceAll("Na22log","2","tu") returns "Natutulog"
std::string normalize :: ReplaceAll(std::string str, const std::string& from, const std::string& to) {
    if(to == "REPETITION"){
    	return repeat_words(str);
    }
    else{
    	size_t start_pos = 0;
    	while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        	str.replace(start_pos, from.length(), to);
        	start_pos += to.length(); 
    	}
    return str;
	}
}
// convert numbers to words: requires int
std::string number_to_words( const unsigned int numero){
    const std::vector<std::string> isa_sampu = {"sero", "isa", "dalawa", "tatlo", "apat", "lima", "anim", "pito", "walo", "siyam", "sampu"};
    const std::vector<std::string> pang_dugtong = {"dalawam", "tatlum", "apatna", "limam", "animna", "pitum", "walum", "siyamna"};
    std::string to_concat;
    if( numero <= 10 ) // 0-10
        return isa_sampu.at(numero);
    if( numero < 20 ){ // 11 - 19
        std::string at_less = isa_sampu.at(numero-10);
        if(at_less == "isa" || at_less == "apat" || at_less == "anim")
            to_concat = "labing-";
        else
            to_concat = "labin";
        return to_concat + isa_sampu.at(numero-10);
    }
    if( numero < 100 ) { // 20 - 99
        unsigned int remainder = numero - (static_cast<int>(numero/10)*10);
        return pang_dugtong.at(numero/10-2) + (0 != remainder ? "pu't " + number_to_words(remainder) : "pu");
    }
    if( numero < 1000 ) { // hundreds
        unsigned int remainder = numero - (static_cast<int>(numero/100)*100);
        std::string numm = isa_sampu.at(numero/100);
        if(numm == "apat" || numm == "anim" || numm == "siyam")
            to_concat = " na raa";
        else
            to_concat = "ng daa";
        return isa_sampu.at(numero/100) + (0 != remainder ? to_concat+"'t " + number_to_words(remainder) : to_concat+"n");
    }
    if( numero < 1000000 ) { // thousands
        unsigned int thousands = static_cast<int>(numero/1000);
        unsigned int remainder = numero - (thousands*1000);
    
        if(thousands == 4 || thousands == 6 || thousands == 9)
            to_concat = " na libo";
        else
            to_concat = "ng libo";
        return number_to_words(thousands) + (0 != remainder ? to_concat+"'t " + number_to_words(remainder) : to_concat);
    }
    if( numero < 1000000000 ) { // millions
        unsigned int millions = static_cast<int>(numero/1000000);
        unsigned int remainder = numero - (millions*1000000);
        std::string numm = number_to_words(millions);
        
        if(numm == "apat" || numm == "anim" || numm == "siyam")
            to_concat = " na milyong ";
        else
            to_concat = "ng milyon ";
        return numm + (0 != remainder ? to_concat + number_to_words(remainder) : to_concat);
    }
    
}
// RULE BASED: processing numbers in words
std::string normalize:: process_rule_based(std::string to_normalize){
 	std::set<std::string> possible_words_rule,possible_words_rule_2;
 	std::set<std::string>  :: iterator it; // iterator 
 	std::vector< std::pair <std::string,std::string> > :: iterator it_rule; // iterator
 	// applying rule base settings to strings
	for(it_rule=rule_based_data.begin();it_rule!=rule_based_data.end();it_rule++){
		std::string key = (*it_rule).first;
		std::string value = (*it_rule).second;
		int has_key = to_normalize.find(key);
		if(has_key != -1){
			int has_value = value.find(',');
	    	if( has_value != -1){ 
		  		if(possible_words_rule.size() == 0)
		  			possible_words_rule.insert(to_normalize);

		  		for(it=possible_words_rule.begin();it!=possible_words_rule.end();it++){
		  			std::stringstream replace_possibilities(value);
	      			while( replace_possibilities.good()){
			    		std::string substr;
			    		std::getline( replace_possibilities, substr, ',' );
	  	  				possible_words_rule_2.insert(ReplaceAll((*it),key,substr));
		  			}
		  		}
		  		possible_words_rule = possible_words_rule_2;
			}
			else
		 		to_normalize = ReplaceAll(to_normalize,key,value);
		}
	}

	std::string to_return;
	int indi = 0;
	// check every possible words create by replacing numbers in words if in dictionary
	for(it=possible_words_rule_2.begin();it!=possible_words_rule_2.end();it++){
		if(search(wordlist,(*it))){
			indi = 1;
			to_return = (*it);
			break;
		}
	}
	if(!indi){ // not in dictionary
		to_normalize = ReplaceAll(to_normalize,"2","to");
		to_normalize = ReplaceAll(to_normalize,"1","i");
		to_normalize = ReplaceAll(to_normalize,"8","e");
		to_normalize = ReplaceAll(to_normalize,"9","g");
		to_normalize = ReplaceAll(to_normalize,"3","3");
		to_normalize = ReplaceAll(to_normalize,"5","s");
		to_return = to_normalize;
	}
	return to_return;
}
// calculate levenstein distance of word#1 and word#2
int normalize :: levenshtein_distance(const std::string &s1, const std::string &s2){
  int s1len = s1.size();
  int s2len = s2.size();
  auto column_start = (decltype(s1len))1;
  auto column = new decltype(s1len)[s1len + 1];
  std::iota(column + column_start - 1, column + s1len + 1, column_start - 1);
  
  for (auto x = column_start; x <= s2len; x++) {
    column[0] = x;
    auto last_diagonal = x - column_start;
    for (auto y = column_start; y <= s1len; y++) {
      auto old_diagonal = column[y];
      auto possibilities = {
        column[y] + 1,
        column[y - 1] + 1,
        last_diagonal + (s1[y - 1] == s2[x - 1]? 0 : 1)
      };
      column[y] = std::min(possibilities);
      last_diagonal = old_diagonal;
    }
  }
  auto result = column[s1len];
  delete[] column;
  return result;
}
// calculate edit distance in each word threshold setting = 1
void normalize :: edit_distance(struct trie *root, char str[], int level, std::string to_normalize,int threshold) {  
    if (root->words != false){ // if end of character of word
        str[level] = '\0';
        if(to_normalize.size() > 3){
        	auto word_from_wordlist = dm::double_metaphone(str);
			auto word_from_user = dm::double_metaphone(to_normalize);
	        // store candidate words to possible_words var
	        if( (levenshtein_distance(str,to_normalize) == threshold) 
	        	|| (word_from_wordlist.first == word_from_user.first ) 
	        	){
	          possible_words.insert(str);
        	}
		}
		else{
			if(levenshtein_distance(str,to_normalize) == threshold){
	          possible_words.insert(str);
        	}
		}
    } // finding words in trie
    for (int i = 0; i < 26; i++)  { 
        if (root->edges[i])  { 
            str[level] = i + 'a'; 
            edit_distance(root->edges[i], str, level + 1,to_normalize,threshold); 
        } 
    }
} 
// produce candidate words
void normalize :: produce_candidate_words(std::string to_normalize,int threshold){
	char str[26];
	while(1){
		edit_distance(wordlist,str,0, to_normalize,threshold); // implement algorithm with edt distance
		if(threshold == 3)
			break;
		else if(possible_words.size() == 0)
			threshold+=1;
		else
			break;
	}
}
// TODO: create a language model using tensorflow, using of model will be placed here
std::string normalize :: language_model(std::set<std::string > possible_words){
	std::set<std::string> :: iterator it;
	std::string to_normalize;
	// the candidate words
	for(it=possible_words.begin();it!=possible_words.end();it++)
		to_normalize= to_normalize +" "+(*it);	
	const char *inputt = to_normalize.c_str();

   	setenv("PYTHONPATH",".",1);
	PyObject *pName, *pModule, *pDict, *pFunc, *pValue, *presult;
	
	Py_Initialize(); // Initialize the Python Interpreter
	pName = PyString_FromString((char*)"get_prob"); // Build the name object
	pModule = PyImport_Import(pName); // Load the module object
	pDict = PyModule_GetDict(pModule);// pDict is a borrowed reference 
	pFunc = PyDict_GetItemString(pDict, (char*)"select_best_word");// pFunc is also a borrowed reference 

	if (PyCallable_Check(pFunc)){
	   pValue=Py_BuildValue("(z)",inputt);
	   PyErr_Print();
	   presult=PyObject_CallObject(pFunc,pValue);
	   PyErr_Print();
	} else {
	   PyErr_Print();
	}
	return PyString_AsString(presult);
	Py_DECREF(pValue);
	// Clean up
	Py_DECREF(pModule);
	Py_DECREF(pName);
	// Finish the Python Interpreter
	Py_Finalize();
}

// main function for processing normalization 
std::string normalize :: normalize_word(std::string to_normalize){ 
	possible_words.clear();
	std::string to_return;
	std::string symbol_concat_last;
	std::string symbol_concat_begin;
	// start measure average time
	auto start_time = std::chrono::high_resolution_clock::now();
	int upper = isupper(to_normalize[0]);
	to_normalize = preprocess(to_normalize);
	if(to_normalize!=""){
		
		//split symbols "?!,."  example: ikaw! -- symbols will be removed and will concatonate later
		int index_last = to_normalize.size()-1;
		if(to_normalize[index_last] == '?' || to_normalize[index_last] == '.'
			|| to_normalize[index_last] == ',' || to_normalize[index_last] == '!'
			|| to_normalize[index_last] == '"'
			|| to_normalize[index_last] == ')' || to_normalize[index_last] == ']'
			|| to_normalize[index_last] == '}'){	
			symbol_concat_last= to_normalize[index_last];
			to_normalize = to_normalize.substr(0, index_last);
		}
		// remove special character in the beginning and concatonate later
		index_last = to_normalize.size();
		if(to_normalize[0] == '"' || to_normalize[0] == '(' || to_normalize[0] == '['
			|| to_normalize[0] == '{'){	
			symbol_concat_begin= to_normalize[0];
			to_normalize = to_normalize.substr(1, index_last);
		}
		// convert to currency
		if(to_normalize.find("php") == 0){
			to_normalize = ReplaceAll(to_normalize,"php","");
			symbol_concat_last= " piso"+symbol_concat_last;
		}
		
		// if input is number then convert to words
		if(is_number(to_normalize)){
			std::string::size_type sz;
			to_return = number_to_words(std::stoi (to_normalize,&sz));
			no_normalized_words_numbers+=1;
		}
		else{
			//process rule based
			to_normalize=process_rule_based(to_normalize); // transform input using rule base
			// check if word is in dictionary
			if( search(wordlist,to_normalize)==0 ){ 
				produce_candidate_words(to_normalize,threshold); // produce candidate words from input
				if(possible_words.size()==0){ // if word does not have normalized form
					to_return = to_normalize;
					unable_to_normalize_words+=1;
				}
				else{
				    no_normalized_words_lstm+=1; // # of candidate words
					//to_return = to_normalize;
					to_return = language_model(possible_words); // has normalized form
				}
			}
			else{ // return if word is in dictionary 
				in_dictionary+=1;
				to_return = to_normalize;
			}
		}
	}
	else
		to_return=to_normalize;
	auto stop_time = std::chrono::high_resolution_clock::now(); // stop time
	std::chrono::duration<double, std::milli> duration = stop_time - start_time; // millise
	average_time_taken+=duration.count(); // add time to global var
	no_words+=1; // no of words
	if(upper) // make uppercase char
		to_return[0] = toupper(to_return[0]);

	return symbol_concat_begin+to_return+symbol_concat_last; // return result with symbol if has "!?,." at the end 
}
// function to normalize a sentence
std::string normalize :: normalize_sentence(std::string sentence){
	std::string normalized_sentence;
    std::istringstream buff(sentence);
    std::istream_iterator<std::string> beg(buff), end;
    std::vector<std::string> tokens(beg, end);

    for(auto& s: tokens)
    	normalized_sentence = normalized_sentence + " " + normalize_word(s);
    return normalized_sentence;
}
// loads a text file and normalize sentence by sentence
void normalize :: normalize_textfile(std::string filename){
	std::ofstream fin_out;
	std::ofstream report;
	std::ifstream  infile;
	infile.open(filename);
	fin_out.open("evaluate/output.txt"); // Filipino-wordlist.txt Filipino_words.txt
	report.open("generated_report.txt");
	
	std::cout << "\n\nTarget file: denormalized.txt" <<std::endl << "Normalization process starting..."<<std::endl;

	std::string line=" ";
	long long count=1;
		while( std::getline(infile, line) ){
			std::istringstream is_line(line);
			std::string sentence = is_line.str();
			std::string normalized_form = normalize_sentence(sentence)+"\n";
			fin_out << normalized_form;
			std::cout << "Processing sentence #" << count<<std::endl;
			count++;
		}
	std::cout << "\n\nNormalization finished, output file: output.txt\n\n";
	report << report_model();
}