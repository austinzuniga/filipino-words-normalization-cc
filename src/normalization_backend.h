#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <set>
#include <fstream>
#include <sstream>
#include <chrono>
#include <cstring>
#include <cctype>
#include <Python.h>
#include <iterator>

// Normalize Class
class normalize{
	long long total_words;
	struct trie{
		long long words;
		trie *edges[26];
			
		trie(){ // Trie struct
			words=0;
			for(int i=0;i<26;i++){
				edges[i]=NULL;
			}
		}
	};
	
	
	public: // initialize all function to class
		normalize(); // constructor
		void load_rule_base();
		
		std::string error_model();
		std::string average_time();
		std::string report_model();
		
		std::string preprocess(std::string word);
		void add_word(struct trie *wordlist,std::string);
		long long search(struct trie *wordlist,std::string);

		std::string process_rule_based(std::string to_normalize);
		int levenshtein_distance(const std::string &s1, const std::string &s2);
		void edit_distance(struct trie *root, char str[], int level, std::string to_normalize, int threshold);
		std::string language_model(std::set<std::string > possible_words);
		void produce_candidate_words(std::string to_normalize,int threshold);

		std::string repeat_words(std::string str);
		std::string ReplaceAll(std::string str, const std::string& from, const std::string& to);

		std::string normalize_word(std::string);
		std::string normalize_sentence(std::string sentence);
		void normalize_textfile(std::string sentence);

		void load_oov_wordlist();

		trie *wordlist;
		trie *oov_wordlist;	
		std::set<std::string>  possible_words; // contains our possible words
		std::vector< std::pair <std::string,std::string> > rule_based_data; // container of rule base data
		int unable_to_normalize_words = 0; // error model 
		long long no_normalized_words_lstm = 0; // success lstm
		long long no_normalized_words_numbers = 0; // success numbers
		long long in_dictionary = 0;  // in dictionary word
		int no_words = 0;
		float average_time_taken = 0;
		int threshold = 1;
};
