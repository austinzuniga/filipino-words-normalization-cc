# Filipino Word Level Normalization in C++
A C++ program that normalize Filipino words using Damerau–Levenshtein distance algorithm, double metaphone algorithm (modified version for Filipino language) and a language model.

#### Confusion set:
Using Damerau–Levenshtein distance algorithm, a list of candidate words will be produced according to the threshold setting (default = 1) and will be added to confusion set.<br>

double metaphone algorithm will also be used to generate candidate words. All words in the dictionary will be processed by the algorithm to get the phonetic code of each word. The phonetic code of the word to be normalized and the words in the dictionary will be compared using the Damerau–Levenshtein distance algorithm. The result will be added to confusion set according to the threshold setting.

#### Selecting Best Word
A Recurrent Neural Network language model will be used to select the best word from the confusion set.

### Installing Modules

##### clone the tensorflow repo: 

	git clone https://github.com/tensorflow/tensorflow.git
<br />
To install tensorflow c++ API, you need to build tensorflow from source: https://www.tensorflow.org/install/source<br /> <br />

You also need to install bazel to build tensorflow: https://bazel.build/o

##### clone the project inside the tensorflow project

	cd tensorflow 
	git clone https://github.com/AustinZuniga/Filipino-Words-Normalization-CC.git

##### Install WxWidget
	Go to: https://www.wxwidgets.org/downloads/

### Run C++ app
	cd Filipino-Words-Normalization-CC 
	chmod +x run.sh 
	./run.sh 

## Screenshot 
![alt text](img/interface.png)

## Modules Used:
	wxWidget 3.1.2 
	tensorflow C++ API 

## Reference:
	working...
