import tensorflow as tf
from tensorflow.contrib import rnn
from tensorflow.contrib import legacy_seq2seq
import random
import numpy as np

class Model():
    def __init__(self, Model_info, infer=False):
        
        if infer:
            Model_info['batch_size'] = 1
            Model_info['seq_length'] = 1

        if Model_info['model'] == 'rnn':
            cell_fn = rnn.BasicRNNCell
        elif Model_info['model'] == 'gru':
            cell_fn = rnn.GRUCell
        elif Model_info['model'] == 'lstm':
            cell_fn = rnn.BasicLSTMCell
        else:
            raise Exception("model type not supported: {}".format(Model_info['model']))

        cells = []
        for _ in range(Model_info['num_layers']):
            cell = cell_fn(Model_info['rnn_size'])
            cells.append(cell)

        self.cell = cell = rnn.MultiRNNCell(cells)

        self.input_data = tf.placeholder(tf.int32, [Model_info['batch_size'], Model_info['seq_length']])
        self.targets = tf.placeholder(tf.int32, [Model_info['batch_size'], Model_info['seq_length']])
        self.initial_state = cell.zero_state(Model_info['batch_size'], tf.float32)
        self.batch_pointer = tf.Variable(0, name="batch_pointer", trainable=False, dtype=tf.int32)
        self.inc_batch_pointer_op = tf.assign(self.batch_pointer, self.batch_pointer + 1)
        self.epoch_pointer = tf.Variable(0, name="epoch_pointer", trainable=False)
        self.batch_time = tf.Variable(0.0, name="batch_time", trainable=False)
        tf.summary.scalar("time_batch", self.batch_time)

        def variable_summaries(var):
            """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
            with tf.name_scope('summaries'):
                mean = tf.reduce_mean(var)
                tf.summary.scalar('mean', mean)
                #with tf.name_scope('stddev'):
                #   stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
                #tf.summary.scalar('stddev', stddev)
                tf.summary.scalar('max', tf.reduce_max(var))
                tf.summary.scalar('min', tf.reduce_min(var))
                #tf.summary.histogram('histogram', var)

        with tf.variable_scope('rnnlm'):
            softmax_w = tf.get_variable("softmax_w", [Model_info['rnn_size'], Model_info['vocab_size']])
            variable_summaries(softmax_w)
            softmax_b = tf.get_variable("softmax_b", [Model_info['vocab_size']])
            variable_summaries(softmax_b)
            with tf.device("/cpu:0"):
                embedding = tf.get_variable("embedding", [Model_info['vocab_size'], Model_info['rnn_size']])
                inputs = tf.split(tf.nn.embedding_lookup(embedding, self.input_data), Model_info['seq_length'], 1)
                inputs = [tf.squeeze(input_, [1]) for input_ in inputs]

        def loop(prev, _):
            prev = tf.matmul(prev, softmax_w) + softmax_b
            prev_symbol = tf.stop_gradient(tf.argmax(prev, 1))
            return tf.nn.embedding_lookup(embedding, prev_symbol)

        outputs, last_state = legacy_seq2seq.rnn_decoder(inputs, self.initial_state, cell, loop_function=loop if infer else None, scope='rnnlm')
        output = tf.reshape(tf.concat(outputs, 1), [-1, Model_info['rnn_size']])
        self.logits = tf.matmul(output, softmax_w) + softmax_b
        self.probs = tf.nn.softmax(self.logits)
        loss = legacy_seq2seq.sequence_loss_by_example([self.logits],
                [tf.reshape(self.targets, [-1])],
                [tf.ones([Model_info['batch_size'] * Model_info['seq_length']])],
                Model_info['vocab_size'])
        self.cost = tf.reduce_sum(loss) / Model_info['batch_size'] / Model_info['seq_length']
        tf.summary.scalar("cost", self.cost)
        self.final_state = last_state
        self.lr = tf.Variable(0.0, trainable=False)
        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(self.cost, tvars),
                Model_info['grad_clip'])
        optimizer = tf.train.AdamOptimizer(self.lr)
        
        self.train_op = optimizer.apply_gradients(zip(grads, tvars))

    def sample(self, sess, vocab, prime='first all'):
        prime_labels = [vocab.get(word, 0) for word in prime.split()]
        return prime_labels