from __future__ import print_function
import numpy as np
import tensorflow as tf

import argparse
import time
import os
from six.moves import cPickle

from utils import TextLoader
from model import Model

def main():

    get_probability = {}
    get_probability['save_dir'] = 'save/'
    get_probability['prime']= "pilipino"
    if sample(get_probability) > 0:
        print("success")

def sample(get_probability):
    with open(os.path.join(get_probability['save_dir'], 'config.pkl'), 'rb') as f:
        saved_args = cPickle.load(f)
    with open(os.path.join(get_probability['save_dir'], 'words_vocab.pkl'), 'rb') as f:
        words, vocab = cPickle.load(f)
    model = Model(saved_args, True)
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        saver = tf.train.Saver(tf.global_variables())
        ckpt = tf.train.get_checkpoint_state(get_probability['save_dir'])
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            for _ in range(1):
              return model.sample(sess,vocab,get_probability['prime'])

if __name__ == '__main__':
    main()
