from __future__ import print_function
import numpy as np
import tensorflow as tf

import argparse
import time
import os
from six.moves import cPickle

from utils import TextLoader
from model import Model

Model_info = {}
Model_info['data_dir'] = 'data/'
Model_info['input_encoding']=None
Model_info['log_dir']='logs/'
Model_info['save_dir'] = 'save/'
Model_info['rnn_size'] = 100
Model_info['num_layers'] = 1
Model_info['model'] = 'lstm'
Model_info['batch_size'] = 32
Model_info['seq_length'] = 25
Model_info['num_epochs'] = 30
Model_info['save_every'] = 1500
Model_info['grad_clip'] = 5.0
Model_info['learning_rate'] = 0.002
Model_info['decay_rate'] = 0.97
Model_info['gpu_mem'] = 0.666
Model_info['init_from'] = 'save/'

data_loader = TextLoader(Model_info['data_dir'], Model_info['batch_size'], Model_info['seq_length'], Model_info['input_encoding'])
Model_info['vocab_size'] = data_loader.vocab_size
if Model_info['init_from'] is not None:
    # check if all necessary files exist
    assert os.path.isdir(Model_info['init_from'])," %s must be a path" % Model_info['init_from']
    assert os.path.isfile(os.path.join(Model_info['init_from'],"config.pkl")),"config.pkl file does not exist in path %s"%Model_info['init_from']
    assert os.path.isfile(os.path.join(Model_info['init_from'],"words_vocab.pkl")),"words_vocab.pkl.pkl file does not exist in path %s" % Model_info['init_from']
    ckpt = tf.train.get_checkpoint_state(Model_info['init_from'])
    assert ckpt,"No checkpoint found"
    assert ckpt.model_checkpoint_path,"No model path found in checkpoint"

    # open old config and check if models are compatible
    with open(os.path.join(Model_info['init_from'], 'config.pkl'), 'rb') as f:
        saved_model_args = cPickle.load(f)
    need_be_same=["model","rnn_size","num_layers","seq_length"]
    #for checkme in need_be_same:
    #    assert vars(saved_model_args)[checkme]==vars(args)[checkme],"Command line argument and saved model disagree on '%s' "%checkme

    # open saved vocab/dict and check if vocabs/dicts are compatible
    with open(os.path.join(Model_info['init_from'], 'words_vocab.pkl'), 'rb') as f:
        saved_words, saved_vocab = cPickle.load(f)
    assert saved_words==data_loader.words, "Data and loaded model disagree on word set!"
    assert saved_vocab==data_loader.vocab, "Data and loaded model disagree on dictionary mappings!"


with open(os.path.join(Model_info['save_dir'], 'config.pkl'), 'wb') as f:
    cPickle.dump(Model_info, f)
with open(os.path.join(Model_info['save_dir'], 'words_vocab.pkl'), 'wb') as f:
    cPickle.dump((data_loader.words, data_loader.vocab), f)

model = Model(Model_info)

merged = tf.summary.merge_all()
train_writer = tf.summary.FileWriter(Model_info['log_dir'])
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=Model_info['gpu_mem'])

with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
    train_writer.add_graph(sess.graph)
    tf.global_variables_initializer().run()
    saver = tf.train.Saver(tf.global_variables())
    # restore model
    if Model_info['init_from'] is not None:
        saver.restore(sess, ckpt.model_checkpoint_path)
    for e in range(model.epoch_pointer.eval(), Model_info['num_epochs']):
        sess.run(tf.assign(model.lr, Model_info['learning_rate'] * (Model_info['decay_rate'] ** e)))
        data_loader.reset_batch_pointer()
        state = sess.run(model.initial_state)
        speed = 0
        if Model_info['init_from'] is None:
            assign_op = model.epoch_pointer.assign(e)
            sess.run(assign_op)
        if Model_info['init_from'] is not None:
            data_loader.pointer = model.batch_pointer.eval()
            Model_info['init_from'] = None
        for b in range(data_loader.pointer, data_loader.num_batches):
            start = time.time()
            x, y = data_loader.next_batch()

            feed = {model.input_data: x, model.targets: y, model.initial_state: state,
                    model.batch_time: speed}

            #accuracy, accuracy_update_op  = tf.metrics.accuracy(labels = tf.argmax(y, axis = 1), predictions = tf.argmax(tf.reshape(model.probs, [50, -1]), axis = 1), name = 'accuracy')
            #running_vars_accuracy = tf.get_collection(tf.GraphKeys.LOCAL_VARIABLES, scope="LSTM/Accuracy")
             
            accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.reshape(tf.cast(tf.argmax(model.probs, axis=1), tf.int32), [32, -1]),model.targets), tf.float32))     
                
            summary, train_loss, state, _, _ = sess.run([merged, model.cost, model.final_state,
                                                         model.train_op, model.inc_batch_pointer_op], feed)

            train_writer.add_summary(summary, e * data_loader.num_batches + b)
            speed = time.time() - start
            

            if (e * data_loader.num_batches + b) % Model_info['batch_size'] == 0:
                accuracy_f = sess.run(accuracy,feed)
                print("{}/{} (epoch {}), train_loss = {:.3f}, time/batch = {:.3f}, accuracy={:.3f}" \
                    .format(e * data_loader.num_batches + b,
                            Model_info['num_epochs'] * data_loader.num_batches,
                            e, train_loss, speed,accuracy_f))
            if (e * data_loader.num_batches + b) % Model_info['save_every'] == 0 \
                    or (e==Model_info['num_epochs']-1 and b == data_loader.num_batches-1): # save for the last result
                checkpoint_path = os.path.join(Model_info['save_dir'], 'model.ckpt')
                saver.save(sess, checkpoint_path, global_step = e * data_loader.num_batches + b)
                print("model saved to {}".format(checkpoint_path))
    train_writer.close()
