g++ -I/usr/include/python2.7 accuracy.cc -g -Wall -Wextra -pedantic src/normalization_backend.cc -lpython2.7 -o evaluate.exe
gdb ./evaluate.exe
cd evaluate
python bleu.py
#bazel run -c opt //Filipino-Words-Normalization-CC:rnn
