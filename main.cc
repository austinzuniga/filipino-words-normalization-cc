/*TODO: 
	rule based settings
	Rationale
	User Manual
	The Team */
#include<Python.h>
#include <string> 
#include <wx/wxprec.h>
#include <wx/wx.h>
// for file
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
// number
#include <wx/spinctrl.h>

#include <wx/grid.h>
#include <wx/srchctrl.h>

#include "src/normalization_backend.h"

// initialize class normalize
normalize* normalization_process = new normalize();
// initialize app
class Normalization_app : public wxApp{
  	public:
    	virtual bool OnInit();
};

//////// main frame
class MainFrame : public wxFrame{
public:
  	MainFrame(const wxString& title);
  	// file
	void OnExit(wxCommandEvent& event);
	//config
	void OnDictionary(wxCommandEvent& event);
	void OnDictionary_OOV(wxCommandEvent& event);

	void Rule_based(wxCommandEvent& event);
	
	//help
	void OnHelp(wxCommandEvent& event);
	//about
	void OnAbout(wxCommandEvent& event);
	void OnTeam(wxCommandEvent& event);
	// normalize button
	void OnClick(wxCommandEvent& event);
	
	// global var for textbox
	wxTextCtrl *textbox_1;
	wxTextCtrl *textbox_2;

private:
	wxDECLARE_EVENT_TABLE();
};

//////// config toolbar
class Dictionary : public wxFrame{
public:
  	Dictionary(const wxString& title);

  	void Quit(wxCommandEvent& event);
  	void OpenFile(wxCommandEvent& event);
  	void SaveFile(wxCommandEvent& event);
  	void CloseFile(wxCommandEvent& event);
  	void AddWord(wxCommandEvent& event);

  	wxTextCtrl *dictionary_textbox;
  	wxTextCtrl *add_word;
  	wxString CurrentDocPath;
private:
  	wxDECLARE_EVENT_TABLE();
};

class OOV_Dictionary : public wxFrame{
public:
  	OOV_Dictionary(const wxString& title);

  	void Quit(wxCommandEvent& event);
  	void OpenFile(wxCommandEvent& event);
  	void SaveFile(wxCommandEvent& event);
  	void CloseFile(wxCommandEvent& event);
  	void AddWord(wxCommandEvent& event);

  	wxTextCtrl *dictionary_textbox;
  	wxTextCtrl *add_word;
  	wxString CurrentDocPath;
private:
  	wxDECLARE_EVENT_TABLE();
};

class Rule : public wxFrame{
public:
  	Rule(const wxString& title);
  	void Save_config(wxCommandEvent& event);
  	
  	wxTextCtrl *rule_based_textbox;
  	wxString CurrentDocPath;
 
private:
  	wxDECLARE_EVENT_TABLE();
};


class Threshold : public wxFrame{
public:
  	Threshold(const wxString& title);
  	void Onchange(wxCommandEvent& WXUNUSED(event));
  	wxSpinCtrlDouble *spin;
};

//////// for Help toolbar
class UserManual : public wxFrame{
public:
  	UserManual(const wxString& title);
};

//////// about toolbar
class Rationale : public wxFrame{
public:
	Rationale(const wxString& title);
};

class TheTeam : public wxFrame{
public:
	TheTeam(const wxString& title);
};

DECLARE_APP(Normalization_app)
IMPLEMENT_APP(Normalization_app)

// declaration of Variable
enum{
    wxID_DICTIONARY,
    wxID_TEAM,
    wxID_Normalize_button,
    wxID_RULE,
    TEXT_Main = wxID_HIGHEST + 1,
  	Dictionary_Open,
  	Dictionary_Open_oov,
  	Dictionary_Close,
  	Dictionary_Save,
  	Dictionary_Quit,
  	Dictionary_add_button,
  	wxID_threshold_btn,
  	Config_Save
};

// Initialize app
bool Normalization_app::OnInit(){
	MainFrame *mainFrame = new MainFrame(wxT("Filipino Word Normalization")); // create frame and title
	mainFrame->Show(true);
	return true;
}

// For Dictionary Frame
BEGIN_EVENT_TABLE (Dictionary, wxFrame ) 
	EVT_MENU(Dictionary_Close, Dictionary::CloseFile)
	EVT_MENU(Dictionary_Save, Dictionary::SaveFile)	
	EVT_MENU(Dictionary_Quit, Dictionary::Quit)
	EVT_MENU(Dictionary_add_button, Dictionary::AddWord)

END_EVENT_TABLE()

//------------- ACTIONS-----------
// opening text file
void Dictionary::OpenFile(wxCommandEvent& WXUNUSED(event)){
  	wxFileDialog *OpenDialog = new wxFileDialog(
    this,("Choose a file to open"), wxEmptyString, wxEmptyString,("Text files (*.txt)|*.txt"),
    wxFD_OPEN, wxDefaultPosition);

  	// Creates a "open file" dialog
	if (OpenDialog->ShowModal() == wxID_OK){
    	CurrentDocPath = "corpus/Wordlist/Filipino_wordlist.txt";
    	// Sets our current document to the file the user selected
    	dictionary_textbox->LoadFile(OpenDialog->GetPath()); //Opens that file	
    	SetTitle(wxString("Edit - ") << OpenDialog->GetFilename()); // Set the Title to reflect the file open
   	}
}
// close file
void Dictionary::CloseFile(wxCommandEvent& WXUNUSED(event)){
	dictionary_textbox->Clear(); // Clear the Text Box
	CurrentDocPath = wxT("corpus/Wordlist/Filipino_wordlist.txt");// set doc path
	SetTitle(("Edit - untitled *")); // Set the Title to reflect the file open
}
// saving dictionary
void Dictionary::SaveFile(wxCommandEvent& WXUNUSED(event)){
	dictionary_textbox->SaveFile(CurrentDocPath);// Save to the already-set path for the document
	delete normalization_process; // delete current object 
	normalization_process = new normalize; // create new object so trie will be updated
}
// Close the window
void Dictionary::Quit(wxCommandEvent& WXUNUSED(event)){
	Close(TRUE); 
}
void Dictionary::AddWord(wxCommandEvent& WXUNUSED(event)){
	 wxString input_from_wx(add_word->GetValue()); // get Value from textbox
	 std::string Input_user_f = std::string(input_from_wx.mb_str());
	 if(normalization_process->search(normalization_process->wordlist,Input_user_f) ||
	 	normalization_process->search(normalization_process->oov_wordlist,Input_user_f))
	 	wxLogMessage("'"+input_from_wx+"' already in Dictionary or word is an OOV word");
	 else
	 	dictionary_textbox->AppendText(input_from_wx);
}
//////////////////////////////// The Dictionary Frame///////////////////////////
Dictionary::Dictionary(const wxString& title):wxFrame(NULL, -1, title, wxPoint(-1, -1), wxSize(700, 500)){
	wxMenuBar *MainMenu;
	MainMenu = new wxMenuBar();
	wxMenu *FileMenu = new wxMenu();

	FileMenu->AppendSeparator();
	FileMenu->Append(Dictionary_Close,"&Close\tCtrl-g",
	                 "Close Dictionary");
	FileMenu->AppendSeparator();

	FileMenu->Append(Dictionary_Save,"&Save\tCtrl-S",
	                 "Save Dictionary");
	FileMenu->AppendSeparator();

	FileMenu->Append(Dictionary_Quit,"&Quit\tCtrl-q",
	                 "Exit Editor");

	MainMenu->Append(FileMenu, ("&File"));
	SetMenuBar(MainMenu);

	wxPanel *panel = new wxPanel(this, -1);

	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer *hbox2 = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *st2 = new wxStaticText(panel, wxID_ANY, 
	  wxT("Dictionary:"));

	hbox2->Add(st2, 0);
	vbox->Add(hbox2, 0, wxLEFT | wxTOP, 10);

	vbox->Add(-1, 10);

	wxBoxSizer *hbox3 = new wxBoxSizer(wxHORIZONTAL);
	dictionary_textbox = new wxTextCtrl(panel, wxID_ANY, wxT(""), 
	  wxPoint(-1, -1), wxSize(-1, -1), wxTE_MULTILINE);

	hbox3->Add(dictionary_textbox, 1, wxEXPAND);
	vbox->Add(hbox3, 1, wxLEFT | wxRIGHT | wxEXPAND, 10);
  	vbox->Add(-1, 25);
  	
  	CurrentDocPath = "corpus/Wordlist/Filipino_wordlist.txt";
	wxFileInputStream input(wxT("corpus/Wordlist/Filipino_wordlist.txt"));
	wxTextInputStream text(input, wxT("\x09"), wxConvUTF8 );
	wxString line;
	int line_count=0;
	while(input.IsOk() && !input.Eof() ){
		line+=text.ReadLine();
		line_count++;
		line+="\n";
	}
	
	std::string line_no = std::to_string(line_count);
	dictionary_textbox->AppendText(line);

	wxBoxSizer *hbox1 = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *st1 =  new wxStaticText(panel, wxID_ANY, 
	  wxT("Add New word"));

	hbox1->Add(st1, 0, wxRIGHT, 8);
	
	vbox->Add(-1, 10);
	
	add_word = new wxTextCtrl(panel, wxID_ANY);
	hbox1->Add(add_word, 1);
	vbox->Add(hbox1, 0,  wxLEFT | wxRIGHT | wxTOP, 3);

	vbox->Add(-1, 10);

	wxBoxSizer *hbox5 = new wxBoxSizer(wxHORIZONTAL);
	wxButton *add_word_button = new wxButton(panel, Dictionary_add_button, wxT("Ok"));
	hbox5->Add(add_word_button, 0);
	vbox->Add(hbox5, 0, wxALIGN_LEFT | wxLEFT, 10);
	Connect(Dictionary_add_button, wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(Dictionary::AddWord));

	panel->SetSizer(vbox);

	CreateStatusBar(); // create status bar (bottom left side)
	SetStatusText(line_no+" words in dictionary"); // status text
}

// For Dictionary Frame
BEGIN_EVENT_TABLE (OOV_Dictionary, wxFrame ) 
	EVT_MENU(Dictionary_Close, OOV_Dictionary::CloseFile)
	EVT_MENU(Dictionary_Save, OOV_Dictionary::SaveFile)	
	EVT_MENU(Dictionary_Quit, OOV_Dictionary::Quit)
	EVT_MENU(Dictionary_add_button, OOV_Dictionary::AddWord)

END_EVENT_TABLE()

///////////////////////////
void OOV_Dictionary::OpenFile(wxCommandEvent& WXUNUSED(event)){
  	wxFileDialog *OpenDialog = new wxFileDialog(
    this,("Choose a file to open"), wxEmptyString, wxEmptyString,("Text files (*.txt)|*.txt"),
    wxFD_OPEN, wxDefaultPosition);

  	// Creates a "open file" dialog
	if (OpenDialog->ShowModal() == wxID_OK){
    	CurrentDocPath = "corpus/Wordlist/Filipino_wordlist_OOV.txt";
    	// Sets our current document to the file the user selected
    	dictionary_textbox->LoadFile(OpenDialog->GetPath()); //Opens that file	
    	SetTitle(wxString("Edit - ") << OpenDialog->GetFilename()); // Set the Title to reflect the file open
   	}
}
// close file
void OOV_Dictionary::CloseFile(wxCommandEvent& WXUNUSED(event)){
	dictionary_textbox->Clear(); // Clear the Text Box
	CurrentDocPath = wxT("corpus/Wordlist/Filipino_wordlist_OOV.txt");// set doc path
	SetTitle(("Edit - untitled *")); // Set the Title to reflect the file open
}
// saving dictionary
void OOV_Dictionary::SaveFile(wxCommandEvent& WXUNUSED(event)){
	dictionary_textbox->SaveFile(CurrentDocPath);// Save to the already-set path for the document
	delete normalization_process; // delete current object 
	normalization_process = new normalize; // create new object so trie will be updated
}
// Close the window
void OOV_Dictionary::Quit(wxCommandEvent& WXUNUSED(event)){
	Close(TRUE); 
}
void OOV_Dictionary::AddWord(wxCommandEvent& WXUNUSED(event)){
	 wxString input_from_wx(add_word->GetValue()); // get Value from textbox
	 std::string Input_user_f = std::string(input_from_wx.mb_str());
	 if(normalization_process->search(normalization_process->wordlist,Input_user_f) ||
	 	normalization_process->search(normalization_process->oov_wordlist,Input_user_f))
	 	wxLogMessage(input_from_wx+" Already in OOV Dictionary or word is in vocabulary words");
	 else
	 	dictionary_textbox->AppendText(input_from_wx);
}
//////////////////////////////// The Dictionary Frame///////////////////////////
OOV_Dictionary::OOV_Dictionary(const wxString& title):wxFrame(NULL, -1, title, wxPoint(-1, -1), wxSize(700, 500)){
	wxMenuBar *MainMenu;
	MainMenu = new wxMenuBar();
	wxMenu *FileMenu = new wxMenu();

	FileMenu->AppendSeparator();
	FileMenu->Append(Dictionary_Close,"&Close\tCtrl-g",
	                 "Close Dictionary");
	FileMenu->AppendSeparator();

	FileMenu->Append(Dictionary_Save,"&Save\tCtrl-S",
	                 "Save Dictionary");
	FileMenu->AppendSeparator();

	FileMenu->Append(Dictionary_Quit,"&Quit\tCtrl-q",
	                 "Exit Editor");

	MainMenu->Append(FileMenu, ("&File"));
	SetMenuBar(MainMenu);

	wxPanel *panel = new wxPanel(this, -1);

	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer *hbox2 = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *st2 = new wxStaticText(panel, wxID_ANY, 
	  wxT("Dictionary:"));

	hbox2->Add(st2, 0);
	vbox->Add(hbox2, 0, wxLEFT | wxTOP, 10);

	vbox->Add(-1, 10);

	wxBoxSizer *hbox3 = new wxBoxSizer(wxHORIZONTAL);
	dictionary_textbox = new wxTextCtrl(panel, wxID_ANY, wxT(""), 
	  wxPoint(-1, -1), wxSize(-1, -1), wxTE_MULTILINE);

	hbox3->Add(dictionary_textbox, 1, wxEXPAND);
	vbox->Add(hbox3, 1, wxLEFT | wxRIGHT | wxEXPAND, 10);
  	vbox->Add(-1, 25);
  	
  	CurrentDocPath = "corpus/Wordlist/Filipino_wordlist_OOV.txt";
	wxFileInputStream input(wxT("corpus/Wordlist/Filipino_wordlist_OOV.txt"));
	wxTextInputStream text(input, wxT("\x09"), wxConvUTF8 );
	wxString line;
	int line_count=0;
	while(input.IsOk() && !input.Eof() ){
		line+=text.ReadLine();
		line_count++;
		line+="\n";
	}
	
	std::string line_no = std::to_string(line_count);
	dictionary_textbox->AppendText(line);

	wxBoxSizer *hbox1 = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *st1 =  new wxStaticText(panel, wxID_ANY, 
	  wxT("Add New word"));

	hbox1->Add(st1, 0, wxRIGHT, 8);
	
	vbox->Add(-1, 10);
	
	add_word = new wxTextCtrl(panel, wxID_ANY);
	hbox1->Add(add_word, 1);
	vbox->Add(hbox1, 0,  wxLEFT | wxRIGHT | wxTOP, 3);

	vbox->Add(-1, 10);

	wxBoxSizer *hbox5 = new wxBoxSizer(wxHORIZONTAL);
	wxButton *add_word_button = new wxButton(panel, Dictionary_add_button, wxT("Ok"));
	hbox5->Add(add_word_button, 0);
	vbox->Add(hbox5, 0, wxALIGN_LEFT | wxLEFT, 10);
	Connect(Dictionary_add_button, wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(Dictionary::AddWord));

	panel->SetSizer(vbox);

	CreateStatusBar(); // create status bar (bottom left side)
	SetStatusText(line_no+" words in dictionary"); // status text
}

// For Rule base Frame
BEGIN_EVENT_TABLE (Rule, wxFrame ) 
	 EVT_MENU(Config_Save, Rule::Save_config)	
END_EVENT_TABLE()

// saving configuration
void Rule::Save_config(wxCommandEvent& WXUNUSED(event)){
	rule_based_textbox->SaveFile(CurrentDocPath);// Save to the already-set path for the document
	delete normalization_process; // delete current object 
	normalization_process = new normalize; // create new object so trie will be updated
}

//////////////////////////////// The User Manual Frame///////////////////////////
Rule::Rule(const wxString& title):wxFrame(NULL, -1, title, wxPoint(-1, -1), wxSize(500, 400)){
	wxMenuBar *MainMenu;
	MainMenu = new wxMenuBar();
	wxMenu *FileMenu = new wxMenu();

	FileMenu->AppendSeparator();
	FileMenu->Append(Config_Save,"&Save\tCtrl-S",
	                 "Save Configuration");
	FileMenu->AppendSeparator();
	MainMenu->Append(FileMenu, ("&File"));
	SetMenuBar(MainMenu);

	rule_based_textbox = new wxTextCtrl(this, TEXT_Main, (""), wxDefaultPosition, wxDefaultSize, 
	wxTE_MULTILINE | wxTE_RICH , wxDefaultValidator, wxTextCtrlNameStr);

	CurrentDocPath = "rule_based.cfg";
	wxFileInputStream input(wxT("rule_based.cfg"));
	wxTextInputStream text(input, wxT("\x09"), wxConvUTF8 );
	wxString line;
	int line_count=0;
	while(input.IsOk() && !input.Eof() ){
		line+=text.ReadLine();
		line_count++;
		line+="\n";
	}
	std::string line_no = std::to_string(line_count);
	rule_based_textbox->AppendText(line);
	CreateStatusBar(); // create status bar (bottom left side)
	SetStatusText("config"); // status text
}

//////////////////////////////// The User Manual Frame///////////////////////////
UserManual::UserManual(const wxString& title):wxFrame(NULL, -1, title, wxPoint(-1, -1), wxSize(300, 200)){
	wxPanel *panel = new wxPanel(this, -1);
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	// The text manual
	wxBoxSizer *hbox0 = new wxBoxSizer(wxHORIZONTAL);
	std::string manual_text = "User Manual:\n\n1. Dictionary\n\t1.1Opening Dictionary:\n\t\tgo to Config->Dictionary to show the Dictionary (wordlist).\n 1.2 Add New words\n\t\t To add words, just open the Dictionary (1.1) and add Filipino words.\n1.3 Import Wordlist\n\t\t To import wordlist, the file format must be a text file (.txt). Go toFile->Import then select file to be imported.";
	wxString test(manual_text);
	wxStaticText *st1 =  new wxStaticText(panel, wxID_ANY,test);
	hbox0->Add(st1, 0);
	vbox->Add(hbox0, 0, wxLEFT | wxTOP, 10);
	vbox->Add(-1, 10);

	panel->SetSizer(vbox);
	Centre();
}

//////////////////////////////// The Team Frame///////////////////////////
TheTeam::TheTeam(const wxString& title):wxFrame(NULL, -1, title, wxPoint(-1, -1), wxSize(400, 300)){
	wxPanel *panel = new wxPanel(this, -1);
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	// Display text
	wxBoxSizer *hbox0 = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *theteam_text =  new wxStaticText(panel, wxID_ANY, 
	  wxT("The Team: "));
	hbox0->Add(theteam_text, 0);
	vbox->Add(hbox0, 0, wxLEFT | wxTOP, 10);
	vbox->Add(-1, 10);

	panel->SetSizer(vbox); // implement design
	Centre(); // center screen
}

//////////////////////////////// The Rationale Frame///////////////////////////
Rationale::Rationale(const wxString& title):wxFrame(NULL, -1, title, wxPoint(-1, -1), wxSize(400, 300)){
	wxPanel *panel = new wxPanel(this, -1);
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	// Display text
	wxBoxSizer *hbox0 = new wxBoxSizer(wxHORIZONTAL);
	wxStaticText *rationale_text =  new wxStaticText(panel, wxID_ANY, 
	  wxT("Rationale: \n\n To Graduate...\n\n We have no other choice."));
	hbox0->Add(rationale_text, 0);
	vbox->Add(hbox0, 0, wxLEFT | wxTOP, 10);
	vbox->Add(-1, 10);

	panel->SetSizer(vbox); // implement design
	Centre();
}


BEGIN_EVENT_TABLE(MainFrame, wxFrame)
	EVT_MENU(wxID_EXIT,  MainFrame::OnExit)
	EVT_MENU(wxID_DICTIONARY,  MainFrame::OnDictionary)
	EVT_MENU(Dictionary_Open, Dictionary::OpenFile)
	EVT_MENU(Dictionary_Open_oov, MainFrame::OnDictionary_OOV)
	EVT_MENU(wxID_RULE,  MainFrame::Rule_based)
	EVT_MENU(wxID_HELP, MainFrame::OnHelp)
	EVT_MENU(wxID_ABOUT, MainFrame::OnAbout)
	EVT_MENU(wxID_TEAM, MainFrame::OnTeam)
END_EVENT_TABLE()

//------------- ACTIONS: MainFrame Funtions-----------

void MainFrame::OnExit(wxCommandEvent& event){
    Close(true); // exit program
}
// Config menu
void MainFrame::OnDictionary(wxCommandEvent& event){
    Dictionary *dictionary = new Dictionary(wxT("Dictionary Settings"));
   	dictionary->Show(true); // Show Dictionary Frame
}

void MainFrame::OnDictionary_OOV(wxCommandEvent& event){
    OOV_Dictionary *dictionary = new OOV_Dictionary(wxT("Dictionary Settings"));
   	dictionary->Show(true); // Show Dictionary Frame
}


void MainFrame::Rule_based(wxCommandEvent& event){
    Rule *rule = new Rule(wxT("Rule Based Settings"));
   	rule->Show(true); // Show Threshold frame
}

// help menu
void MainFrame::OnHelp(wxCommandEvent& event){
    UserManual *userManual = new UserManual(wxT("User Manual"));
   	userManual->Show(true); // show User Manual Frame
}

// About menu
void MainFrame::OnAbout(wxCommandEvent& event){
    Rationale *rationale = new Rationale(wxT("Rationale"));
   	rationale->Show(true); // show Rationale Frame
}
void MainFrame::OnTeam(wxCommandEvent& event){
    TheTeam *theTeam = new TheTeam(wxT("The Team"));
   	theTeam->Show(true); // The Team Fram
}

// normalization button
void MainFrame::OnClick(wxCommandEvent& event){
	textbox_2->Clear(); // clear Textbox
    wxString input_from_wx(textbox_1->GetValue()); // get Value from textbox

	std::string Input_user_f = std::string(input_from_wx.mb_str());

    std::string normalized_word= normalization_process->normalize_sentence(Input_user_f);
    
    wxString error_count(normalization_process->error_model()); // error count
    wxString time_count(normalization_process->average_time()); // average time by word

    SetStatusText("Error count: "+error_count+" ---- Average Time: "+time_count+" ms" );
	// converting string to wxString
	wxString mystring(normalized_word);
	// displaying text to text box
	textbox_2->AppendText(mystring);
}

//////////////////////////////// The Main Frame///////////////////////////
MainFrame::MainFrame(const wxString& title):wxFrame(NULL, -1, title, wxPoint(-1, -1), wxSize(1000, 600)){
  	// file toolbar
	wxMenu *menuFile = new wxMenu;
	menuFile->AppendSeparator(); //seperate line
	menuFile->Append(wxID_EXIT); // exit app button

	// Config Toolbar
	wxMenu *menuDictionary = new wxMenu;
	menuDictionary->Append(wxID_DICTIONARY, "&Edit Dictionary\tCtrl-D",
	                 "Edit Dictionary"); // Dictionary option
	
	menuDictionary->Append(Dictionary_Open, "&Import Dictionary\tCtrl-I",
	                 "Import Dictionary"); // Dictionary option

	menuDictionary->Append(Dictionary_Open_oov, "&OOV Dictionary\tCtrl-I",
	                 "OOV Dictionary"); // Dictionary option

	wxMenu *menuRules = new wxMenu;
	menuRules->Append(wxID_RULE, "&Configure Rule base Settings\tCtrl-R",
	                 "Configure Rule base Settings"); // Threshold option
	// help toolbar
	wxMenu *menuHelp = new wxMenu;
	menuHelp->Append(wxID_HELP,"&User Manual\tCtrl-U","Open User Manual"); // user Manual
	
	//About ToolBar
	wxMenu *menuAbout = new wxMenu;
	menuAbout->Append(wxID_ABOUT,"&Rationale\tCtrl-R",
	                 "About Rationale");
	menuAbout->Append(wxID_TEAM,"&The Team\tCtrl-B",
                 "About Rationale");

	// toolbar name display
	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append( menuFile, "&File" ); // file option
	menuBar->Append( menuDictionary, "&Dictionary" ); // config
	menuBar->Append( menuRules, "&Rules Settings" ); // config
	menuBar->Append( menuHelp, "&Help" ); // Help
	menuBar->Append( menuAbout, "&About" ); // About
	SetMenuBar(menuBar); // Add menu to bar
	// end of toolbar
	
	// Initialize panel design
	wxPanel *panel = new wxPanel(this, -1);
	wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);

	wxGridSizer* gs_text = new wxGridSizer(2);
	// Input Text display
	wxStaticText *input_text = new wxStaticText(panel, wxID_ANY, 
	  wxT("Input Text"));
	gs_text->Add(input_text, 0, wxLEFT | wxTOP, 10);
	// Normalized Text display
	wxStaticText *output_text = new wxStaticText(panel, wxID_ANY, 
	  wxT("Normalized Text"));
	gs_text->Add(output_text, 0,wxLEFT | wxTOP, 10);

	vbox->Add(gs_text, 0, wxEXPAND);

	wxGridSizer* gs= new wxGridSizer(2);
	// Input Text box
	textbox_1 = new wxTextCtrl(panel, wxID_ANY,wxT(""), 
	  wxPoint(-1, -1), wxSize(-1, -1), wxTE_MULTILINE);
	gs->Add(textbox_1, 0, wxEXPAND |  wxLEFT | wxTOP, 10);
	// Normalized Text box
	textbox_2 = new wxTextCtrl(panel, wxID_ANY,wxT(""), 
	  wxPoint(-1, -1), wxSize(-1, -1), wxTE_MULTILINE);
	gs->Add(textbox_2, 0, wxEXPAND |  wxLEFT | wxTOP | wxRIGHT, 10);
	vbox->Add(gs, 1, wxEXPAND);
	
	vbox->Add(-1, 15);

	// the button
	wxBoxSizer *hbox5 = new wxBoxSizer(wxHORIZONTAL);
	wxButton *Normalize_button = new wxButton(panel, wxID_Normalize_button, wxT("Normalize"));
	// event handler: when the button click
	Connect(wxID_Normalize_button, wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(MainFrame::OnClick));
    Normalize_button->SetFocus(); // highlight button when clicked
	hbox5->Add(Normalize_button, 0, wxLEFT | wxBOTTOM , 5);
	vbox->Add(hbox5, 0, wxALIGN_CENTER_HORIZONTAL);

	CreateStatusBar(); // create status bar (bottom left side)
	SetStatusText("Normalize sentence/words using the text box"); // status text

	panel->SetSizer(vbox); // Implement all design
	Centre(); // center screen
}