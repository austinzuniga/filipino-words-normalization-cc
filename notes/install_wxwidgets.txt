Installing WxWidget Windows 10

1. setup minGW-w64
	first download mingw-w64-install.exe here:
		https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/8.1.0/threads-posix/seh/

	choose 8.1 Version


	after installing minGW, set environment variable.
		Computer->properties->advance system settings->environment variables
		in system variables, edit PATH and click new
		insert the bin directory of minGW
			example: C:\minG\mingw64\bin

2. setup mysys
	download mingw-get-setup.exe here:
		 https://sourceforge.net/projects/mingw/files/
	open installer.
	when asked about the installation directory, choose the minGW installation folder (step 1).
	choose only msys-base-bin for installation.
	click Installation (upper left) and choose update catalogue
	done.

3. setup wxwidget
	3.1 download windows installer here:
			https://www.wxwidgets.org/downloads/
	3.2 choose Version 3.1.2
	3.3 open installer
	3.4 when finished, go to environment variables again and create new
			insert the directory of the wxwidget
				example C:\wxWidget
	3.5 navigate to minGW installation folder
			go to mysys/1.0/
				double click mysys.bat
				a command line should appear
	3.6 go to wxWidget installation folder
		example: cd C:/wxWidget
		then type the commands: (note: in running configure,make and make install, it might take some time!!)
			mkdir build-debug
			cd build-debug
			../configure --enable-unicode --enable-debug 
			make
			make install
	done

4. copying DLL to project:
	4.1 first clone the project:
			git clone https://github.com/AustinZuniga/Filipino-Words-Normalization-CC.git
			cd Filipino-Words-Normalization-CC
			copy wxbase312u_gcc_custom.dll and wxmsw312u_core_gcc_custom.dll to project folder.
			location of DLL: C:Wxwidget/build-debug/lib
5. Running project:
	to compile the project:
		g++ main.cc `wx-config --cxxflags --libs` src/normalization_backend.cc src/double_metaphone.cc -o Filipino_normalization.exe
 	run the project:
 			Filipino_normalization.exe	
